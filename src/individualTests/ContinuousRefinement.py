import Config
import ExtractSliceFromVolume
from datasets.HeartDataSet import HeartDataSet
from metrics import SimilarityMetrics
from src.Pyramid import Pyramid
from src.experiments import PyramidSimplex
from individualTests.ResultsManager import IndividualResultsManager

dataSet = HeartDataSet()

numberOfLevels = 4
pyramid3D = Pyramid(dataSet.getVolume().GetDimension(),Config.CONTINUOUS)
pyramid3D.createPyramid(dataSet.getVolumePath(),numberOfLevels,1)

numberOfSeries = 10
numberOfImages = 10
numberOfInitials = 3

for nserie in xrange(numberOfSeries):
    transformation = dataSet.getInitialTransformation(nserie)
    for nslice in xrange(numberOfImages):
        for index in xrange(numberOfInitials):
            resultsManager = IndividualResultsManager()
            if resultsManager.parse(nserie, nslice, index):
                parameters = resultsManager.getDiscreteSolution()
                transformation.SetParameters(parameters)
                refinedSolution = PyramidSimplex.Run(dataSet, nserie, nslice, transformation, numberOfLevels)
                resultsManager.setRefinedSolution(refinedSolution.GetParameters())
                slice = ExtractSliceFromVolume.Execute(dataSet.getVolume(), refinedSolution, dataSet.getGoalSlice(nserie, nslice).GetWidth(), dataSet.getGoalSlice(nserie, nslice).GetHeight(), list(dataSet.getGoalSlice(nserie, nslice).GetSpacing()) + [1], dataSet.getOrigin())      
                resultsManager.refinedSAD = SimilarityMetrics.SumeOfAbsoluteDifferences(dataSet.getGoalSlice(nserie, nslice), slice)
                resultsManager.refinedSSD = SimilarityMetrics.SumeOfSquareDifferences(dataSet.getGoalSlice(nserie, nslice), slice)                
                resultsManager.save(nserie, nslice, index)
            else:
                print "NO file: " + str(nserie) + " " + str(nslice) + " " + str(index)