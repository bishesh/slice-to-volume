import plotly.offline as py
import plotly.graph_objs as go
import src.Config as Config
from serialTests.ResultsManager import SerialResultsManager
from src.analysis.GroundTruth import GroundTruthSlice
import numpy as np

def getNormalizedSAD(sad):
    return sad/14400

def distance(a,b):
    return np.linalg.norm(a-b)
    
numberOfSeries = 3
numberOfImages = 20

for nSerie in xrange(numberOfSeries):
    slices = []
    
    continuousSAD = []
    discreteSAD = []
    refinedSAD = []
    
    continuousDistanceTranslation = []
    discreteDistanceTranslation = []
    refinedDistanceTranslation = []
    
    continuousDistanceRotate = []
    discreteDistanceRotate = []
    refinedDistanceRotate = []
    
    pathGT = Config.DATASET_PATH + "/" + str(nSerie)

    for nSlice in xrange(numberOfImages):
        sliceC = SerialResultsManager()
        sliceD = SerialResultsManager()
        sliceR = SerialResultsManager()
        gt = GroundTruthSlice()
        
        gt.parse(pathGT+"/"+Config.SLICE_FILENAME+str(nSlice+1)+".nfo")
        sliceC.parse(nSerie,nSlice,Config.CONTINUOUS)
        sliceD.parse(nSerie,nSlice,Config.DISCRETE)
        sliceR.parse(nSerie,nSlice,Config.REFINED)
        slices.append("&nbsp;"+str(nSlice+1)+"&nbsp;")

        
        # SAD
        continuousSAD.append(getNormalizedSAD(sliceC.solutionSAD))
        discreteSAD.append(getNormalizedSAD(sliceD.solutionSAD))
        refinedSAD.append(getNormalizedSAD(sliceR.solutionSAD))
        
        # estimated error 
        rotationGT = np.array([gt.transformation['RotateX'],gt.transformation['RotateY'],gt.transformation['RotateZ']])
        rotationC = np.array([sliceC.solution['RotateX'],sliceC.solution['RotateY'],sliceC.solution['RotateZ']])
        rotationD = np.array([sliceD.solution['RotateX'],sliceD.solution['RotateY'],sliceD.solution['RotateZ']])
        rotationR = np.array([sliceR.solution['RotateX'],sliceR.solution['RotateY'],sliceR.solution['RotateZ']])
        
        
        continuousDistanceRotate.append(distance(rotationGT,rotationC))
        discreteDistanceRotate.append(distance(rotationGT,rotationD))
        refinedDistanceRotate.append(distance(rotationGT,rotationR))

        translationGT = np.array([gt.transformation['TranslateX'],gt.transformation['TranslateY'],gt.transformation['TranslateZ']])
        translationC = np.array([sliceC.solution['TranslateX'],sliceC.solution['TranslateY'],sliceC.solution['TranslateZ']])
        translationD = np.array([sliceD.solution['TranslateX'],sliceD.solution['TranslateY'],sliceD.solution['TranslateZ']])
        translationR = np.array([sliceR.solution['TranslateX'],sliceR.solution['TranslateY'],sliceR.solution['TranslateZ']])

        continuousDistanceTranslation.append(distance(translationGT,translationC))
        discreteDistanceTranslation.append(distance(translationGT,translationD))     
        refinedDistanceTranslation.append(distance(translationGT,translationR))        

        
        
    traceC = go.Scatter(
        x = slices,
        y = continuousSAD,
        hoverinfo='y',
        name = "Simplex",
        line = dict(
            color = ('rgb(0, 69, 134)')
        )
    )
        
    traceD = go.Scatter(
        x = slices,
        y = discreteSAD,
        hoverinfo='y',
        name = "Discrete",
        line = dict(
            color = ('rgb(255, 66, 14)')
        )
    )
    
    traceR = go.Scatter(
        x = slices,
        y = refinedSAD,
        hoverinfo='y',
        name = "Refined",
        line = dict(
            color = ('rgb(255, 211, 32)')
        )
    )
    
    data = [traceC,traceD,traceR]
    layout = dict(title = 'SAD (serie: '+str(nSerie)+')',
                  xaxis = dict(title = 'SLICES'),
                  yaxis = dict(title = 'SAD')
    )
    
    fig = dict(data=data, layout=layout)
    py.plot(fig, filename=Config.SERIAL_TEST_PATH+"/plots/"+str(nSerie)+'_sad.html')
   
    
    
    traceC = go.Scatter(
        x = slices,
        y = continuousDistanceRotate,
        hoverinfo='y',
        name = "Simplex",
        line = dict(
            color = ('rgb(0, 69, 134)')
        )
    )
        
    traceD = go.Scatter(
        x = slices,
        y = discreteDistanceRotate,
        hoverinfo='y',
        name = "Discrete",
        line = dict(
            color = ('rgb(255, 66, 14)')
        )
    )
    
    traceR = go.Scatter(
        x = slices,
        y = refinedDistanceRotate,
        hoverinfo='y',
        name = "Refined",
        line = dict(
            color = ('rgb(255, 211, 32)')
        )
    )
    
    data = [traceC,traceD,traceR]
    layout = dict(title = 'ROTATE (serie: '+str(nSerie)+')',
                  xaxis = dict(title = 'SLICES'),
                  yaxis = dict(title = 'Rad')
    )
    
    fig = dict(data=data, layout=layout)
    py.plot(fig, filename=Config.SERIAL_TEST_PATH+"/plots/"+str(nSerie)+'_rotate.html')
    
    
    traceC = go.Scatter(
        x = slices,
        y = continuousDistanceTranslation,
        hoverinfo='y',
        name = "Simplex",
        line = dict(
            color = ('rgb(0, 69, 134)')
        )
    )
        
    traceD = go.Scatter(
        x = slices,
        y = discreteDistanceTranslation,
        hoverinfo='y',
        name = "Discrete",
        line = dict(
            color = ('rgb(255, 66, 14)')
        )
    )
    
    traceR = go.Scatter(
        x = slices,
        y = refinedDistanceTranslation,
        hoverinfo='y',
        name = "Refined",
        line = dict(
            color = ('rgb(255, 211, 32)')
        )
    )
    
    data = [traceC,traceD,traceR]
    layout = dict(title = 'TRANSLATE (serie: '+str(nSerie)+')',
                  xaxis = dict(title = 'SLICES'),
                  yaxis = dict(title = 'mm')
    )
    
    fig = dict(data=data, layout=layout)
    py.plot(fig, filename=Config.SERIAL_TEST_PATH+"/plots/"+str(nSerie)+'_translate.html')

