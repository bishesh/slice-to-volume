import itk
import os
import SimpleITK as sitk
import Config

class Pyramid:
    
    def __init__(self, dimension, name):
        self.__dimension = dimension
        self.__name = name
            
    def createPyramid(self, imagePath, numberOfLevels, zShrinkFactor=None):
        self.__image_type = itk.Image[itk.F, self.__dimension]
        reader = itk.ImageFileReader[self.__image_type].New()
        reader.SetFileName(imagePath)
        reader.Update()
        self.__image = reader.GetOutput()
        self.__numberOfLevels = numberOfLevels
        self.__zShrinkFactor = zShrinkFactor
        
        multi = itk.RecursiveMultiResolutionPyramidImageFilter[self.__image_type,self.__image_type].New()
        multi.SetInput(self.__image)
        multi.SetNumberOfLevels(numberOfLevels)
        
        """
        shrinkFactorsArray = itk.FixedArray[itk.UI,3]
        shrinkFactorsArray.SetElement(0,8)
        shrinkFactorsArray.SetElement(1,8)
        shrinkFactorsArray.SetElement(2,2)
        multi.SetStartingShrinkFactors(shrinkFactorsArray.GetDataPointer())
        #El metodo anterior no puedo hacerlo funcionar, lo que sigue es un parche que hace algo similar       
        """
       
        if (self.__zShrinkFactor) and (self.__dimension is 3):                
            schedule = multi.GetSchedule()
            for level in xrange(self.__numberOfLevels):
                schedule.SetElement(level,2,self.__zShrinkFactor)
            multi.SetSchedule(schedule)
        
        multi.Update()
        
        directory = Config.PYRAMID_PATH+"/"+str(self.__name)
        if not os.path.exists(directory):
            os.makedirs(directory)
        
        for x in xrange(numberOfLevels):
            writer = itk.ImageFileWriter[self.__image_type].New()
            writer.SetFileName(directory+"/dim_"+str(self.__dimension)+"_level_"+str(x)+".mhd")
            writer.SetInput(multi.GetOutput(x))
            writer.Update()
            
    def getLevel(self, level):
        return sitk.ReadImage(Config.PYRAMID_PATH+"/"+str(self.__name)+"/dim_"+str(self.__dimension)+"_level_"+str(level)+".mhd")
