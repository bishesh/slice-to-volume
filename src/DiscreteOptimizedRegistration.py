# -*- coding: utf-8 -*-
import itertools
import numpy as np
import time
import os
from IPython.parallel import Client
import Config
import ExtractSliceFromVolume
from datasets.HeartDataSet import HeartDataSet
from metrics import SimilarityMetrics
from src.Pyramid import Pyramid
from src.experiments import PyramidSimplex
from libfastpd import fastpd
from src.transformations import Rigid3D

# labeling for each kind of parameter
rotationLabels = [0,0,0,0,0,0]
translationLabels = [0,0,0,0,0,0]

# returns the corresponding labeling for the specified parameter
def getCorrespondingLabeling(nVar):
    if nVar < 3:
        return rotationLabels
    else:
        return translationLabels

# generates a labeling like [-'maxValue';'maxValue'] with 'numberOfSteps' values. The first position is always 0 (zero)
def generateLabeling(maxValue, numberOfSteps):
    stepSize = maxValue * 2.0 / (numberOfSteps - 1)
    labels = []
    for nStep in xrange(numberOfSteps):
        currentValue = -maxValue + nStep * stepSize
        if currentValue == 0:
            labels.insert(0,currentValue)
        else:
            labels.append(currentValue)
    if numberOfSteps % 2 == 0:
        labels[0] = 0
    return labels

# generates all the unique pair combinations among the variables
def make_pairs(nVars):
    listvars = np.arange(nVars)
    pairs = list(itertools.combinations(listvars, 2))
    return np.array(pairs, dtype=np.int32)
    
def getUnaries(L, N):
    return np.ones((L, N), dtype=np.float32)

def getBinaryValue(x,y,pair,parameters,level):
    import sys
    # absolute paths for paralelization
    sys.path.append("/home/rocko/python-workspace/Tesis/slice-to-volume/src/")  
    sys.path.append("/home/rocko/python-workspace/Prueba/venv/lib/python2.7/site-packages/SimpleITK-0.9.1-py2.7-linux-x86_64.egg/SimpleITK")    
    
    import ExtractSliceFromVolume
    from metrics import SimilarityMetrics
    from transformations import Rigid3D
    from Pyramid import Pyramid
    import Config
    
    pyramid3D = Pyramid(3,Config.DISCRETE)
    pyramid2D = Pyramid(2,Config.DISCRETE)
    volume = pyramid3D.getLevel(level)
    goalSlice = pyramid2D.getLevel(level)    

    sliceWidth = goalSlice.GetWidth()
    sliceHeight = goalSlice.GetHeight()
    outputSpacing = list(goalSlice.GetSpacing()) + [1]
    origin = (0,0,0)
    
    newParameters = list(parameters)
    newParameters[pair[0]] += x
    newParameters[pair[1]] += y
    
    transformation = Rigid3D.Generate(newParameters)
    slice = ExtractSliceFromVolume.Execute(volume, transformation, sliceWidth, sliceHeight, outputSpacing, origin)
    return SimilarityMetrics.SumeOfSquareDifferences(goalSlice, slice)
    
def getBinaries(parameters, pairs, level):
    rc = Client() # condition, execute in console: "ipcluster start -n 4"
    dview = rc[:]
    binaries = []
    for npair in xrange(len(pairs)):
        pair = pairs[npair]
        var1labels =  getCorrespondingLabeling(pair[0])
        var2labels =  getCorrespondingLabeling(pair[1])
        allVar1, allVar2 = zip(*itertools.product(var1labels, var2labels))
        amr = dview.map(getBinaryValue, allVar1, allVar2, [pair]*len(allVar1), [parameters]*len(allVar1), [level]*len(allVar1))
        b = np.array(list(amr), dtype=np.float32).reshape(len(var1labels),len(var2labels))
        binaries.append(b)
        """ # secuencial
        b = np.ones((len(var1labels), len(var2labels)), dtype=np.float32)
        for labelv1 in xrange(len(var1labels)):
            for labelv2 in xrange(len(var2labels)):   
                newTransformation = initialTransformation.copy()
                newTransformation[pair[0]] += var1labels[labelv1]
                newTransformation[pair[1]] += var2labels[labelv2]
                slice = ExtractSliceFromVolume.Execute(dataSet, newTransformation)
                b[labelv1, labelv2] = SimilarityMetrics.SumeOfSquareDifferences(dataSet.getGoalSlice(serie, image) , slice)
        binaries.append(b)"""
    rc.close()
    return binaries
    
def estimateLabelingTransformation(parameters, maxRotationAngle, maxTranslation, numberSteps, nAlgorithmIter, level):
    global rotationLabels 
    rotationLabels = generateLabeling(maxRotationAngle, numberSteps)
    global translationLabels 
    translationLabels = generateLabeling(maxTranslation, numberSteps)
    nVars = len(parameters)
    pairs = make_pairs(nVars)
    u = getUnaries(numberSteps, nVars)
    b = getBinaries(parameters, pairs, level)
    nit = nAlgorithmIter
    labeling = fastpd(u, b, pairs, nit)  
    estimatedTransformation = list(parameters)
    for nVar in xrange(len(labeling)):
        estimatedTransformation[nVar] += getCorrespondingLabeling(nVar)[labeling[nVar]]
    return estimatedTransformation
    
def runDiscreteSolution(volume, transformation, goalSlice, maxRotationAngle, maxTranslation, nSteps, modificationRate, nAlgorithmIter, nIters, level):
    similarity = 9999999999    
    origin = (0,0,0)
    converge = 0
    for iter in xrange(nIters):
        if converge == 30:
            print "BREAK! iter n: " + str(iter)
            break
        oldParameters = transformation.GetParameters()
        estimatedTransformation = estimateLabelingTransformation(transformation.GetParameters(), maxRotationAngle, maxTranslation, nSteps, nAlgorithmIter, level)
        transformation.SetParameters(estimatedTransformation)     
        sliceWidth = goalSlice.GetWidth()
        sliceHeight = goalSlice.GetHeight()
        outputSpacing = list(goalSlice.GetSpacing()) + [1]
        slice = ExtractSliceFromVolume.Execute(volume, transformation, sliceWidth, sliceHeight, outputSpacing, origin)
        newSimilarity = SimilarityMetrics.SumeOfSquareDifferences(goalSlice, slice)   
        if newSimilarity < similarity:
            similarity = newSimilarity
            converge = 0
        else:
            transformation.SetParameters(oldParameters) 
            converge += 1
        maxRotationAngle -= maxRotationAngle * modificationRate
        maxTranslation -= maxTranslation * modificationRate
    return transformation

def runDiscretePyramidSolution(dataSet, serie, image, transformation, nIters, maxRotationAngle, maxTranslation, modificationRate, nAlgorithmIter):
    volume = dataSet.getVolume()
    goalSlice = dataSet.getGoalSlice(serie,image)  
    pyramid3D = Pyramid(volume.GetDimension(),Config.DISCRETE)
    pyramid2D = Pyramid(goalSlice.GetDimension(),Config.DISCRETE)
    pyramid3D.createPyramid(dataSet.getVolumePath(),numberOfLevels,1)
    pyramid2D.createPyramid(dataSet.getGoalSlicePath(serie,image),numberOfLevels)
    for level in xrange(numberOfLevels):
        volume = pyramid3D.getLevel(level)
        goalSlice = pyramid2D.getLevel(level)        
        transformation = runDiscreteSolution(volume, transformation, goalSlice, maxRotationAngle[level], maxTranslation[level], nSteps, modificationRate[level], nAlgorithmIter, nIters[level], level)
    return transformation
    
def GenerateNsol(initTr, minRot, maxRot, minTr, maxTr, n, experimentName):
    initials = []
    for i in xrange(n):
        initials.append(Rigid3D.GenerateRandomAway(initTr.GetParameters(), minRot, maxRot, minTr, maxTr))
    return initials

def GenerateAwayInitials(transformation, minRndRot, maxRndRot, minRndTr, maxRndTr):
    initials = []
    for i in xrange(len(minRndRot)):
        initials.append(Rigid3D.GenerateRandomAway(transformation.GetParameters(), minRndRot[i], maxRndRot[i], minRndTr[i], maxRndTr[i]))
    return initials
    
np.set_printoptions(precision=3)
np.set_printoptions(suppress=True)

dataSet = HeartDataSet()

nAlgorithmIter = 100
nSteps = 5 # number of labels

# parameters for the discrete iterative strategy
numberOfLevels = 4
nIters = [200, 100, 150, 9999]
maxRotationAngle = [0.02, 0.015, 0.0125, 0.01]
maxTranslation = [7, 6.5, 6, 5]
modificationRate = [0.08, 0.07, 0.05, 0.03]

# experiment for one isolated case:

pyramid3D = Pyramid(dataSet.getVolume().GetDimension(),Config.DISCRETE)
pyramid3D.createPyramid(dataSet.getVolumePath(),numberOfLevels,1)

nserie = 0
nslice = 0

transformation = dataSet.getInitialTransformation(0) # specify the initial transformation. e.g. transformation.SetParameters([0.12, 0.06, 0.14, 22.9, 15.8, 5])
goalSlice = dataSet.getGoalSlice(nserie, nslice) # image that specifies the goal slice
parameters = transformation.GetParameters()  
 
slice = ExtractSliceFromVolume.Execute(dataSet.getVolume(), transformation, goalSlice.GetWidth(), goalSlice.GetHeight(), list(goalSlice.GetSpacing()) + [1], dataSet.getOrigin())
# slice corresponding to the initial transformation
print "Initial error: " + str(SimilarityMetrics.SumeOfSquareDifferences(goalSlice, slice)) # SSD between goal and initial

discreteSolution = runDiscretePyramidSolution(dataSet, nserie, nslice, transformation, nIters, maxRotationAngle, maxTranslation, modificationRate, nAlgorithmIter)
# discrete solution
refinedSolution = PyramidSimplex.Run(dataSet, nserie, nslice, discreteSolution, numberOfLevels)
# refined solution (using simplex)

print "Estimated transformation = " + str(refinedSolution.GetParameters())

slice = ExtractSliceFromVolume.Execute(dataSet.getVolume(), refinedSolution, goalSlice.GetWidth(), goalSlice.GetHeight(), list(goalSlice.GetSpacing()) + [1], dataSet.getOrigin())      
# slice corresponding to the refined solution transformation

print "Estimated error: " + str(SimilarityMetrics.SumeOfSquareDifferences(goalSlice, slice))

