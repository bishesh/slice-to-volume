# Installing and configuring the environment

## Installing dependencies
To install all the libraries, we recommend to install and activate a virtual environment (virtualenv) for the project. 

### SimpleITK
There are many SimpleITK distributions, we suggest Whells or Eggs.

###  Wheels
```$ pip install  --trusted-host www.simpleitk.org -f http://www.simpleitk.org/SimpleITK/resources/software.html --timeout 30 SimpleITK```

### Eggs
```$ easy_install -f http://www.simpleitk.org/SimpleITK/resources/software.html -U SimpleITK```

### ITK
SimpleITK does not include the pyramidal filters that we need for the pyramidal approach, that is why it is necessary to install also the complete version library ITK. For a detailed installation guide, see this link https://itk.org/Wiki/ITK/WrapITKInstallLinux

### Numpy/Scipy
The code for the experiments uses the scientific library ScyPy. To install it in Ubuntu or Debian, execute:

```sudo apt-get install python-numpy python-scipy python-matplotlib ipython ipython-notebook python-pandas python-sympy python-nose```

For other Linux versions, Win or Mac, follow this link: https://www.scipy.org/install.html

## Setting config.py file
It is necessary to configure some absolute paths in the file Config.py in order to use the dataset for the experiments:

```
FILE_NAME3D = "<your_path>/dataset/Heart/3D/im_t1.mhd"
DATASET_PATH = "<your_path>/dataset/Heart/2D"
PYRAMID_PATH = "<your_path>/pyramid"
INDIVIDUAL_TEST_PATH = "<your_path>/IndividualTests"
SERIAL_TEST_PATH = "<your_path>/SerialTests"
OUTPUT_PATH = "<your_path>/dataset/registracion"
```

## Parallelization settings
Some critical parts of the code have been parallelized in order to reduce the execution time. To enable this parallelization it is necessary to follow the next steps:

1. The method DiscreteOptimizedRegistration::getBinaryValue(...) must include the absolute paths for the project and the SimpleITK library.
2. Before run the experiments, the parallelization must be enabled from console, executing:

`$ ipcluster start -n <number_of_processors>`

For more info about parallelization in Python, see this link: https://www.wakari.io/sharing/bundle/ijstokes/ipcluster-wakari-intro

## Dataset
The heart dataset used for the experiments has a folder structure and specific image properties (filename, width, height, etc.) that are used in the class datasets/HeartDataSet.py in order to run the experiments. To use a different dataset, the user must specify in that class (or in a new one) the particularities of the new dataset.

## Running the script
At this point, the environment and the project were installed and configured, so we are ready to run the experiments.

### DiscreteOptimizedRegistration.py

Initially, the script is prepared to run one isolated experiment, and by default the parameters are set with the configuration used to run the experiments for the workshop, but the user can modify them:

```
numberOfLevels = 4
nIters = [200, 100, 150, 9999]
maxRotationAngle = [0.02, 0.015, 0.0125, 0.01]
maxTranslation = [7, 6.5, 6, 5]
modificationRate = [0.08, 0.07, 0.05, 0.03]
```

Now, the script is ready to run the experiment. It can be done from the Python IDE o directly from the console:
```$ python DiscreteOptimizedRegistration.py```